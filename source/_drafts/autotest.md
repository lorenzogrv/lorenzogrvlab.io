---
title: autotest
date: 2020-12-07 17:28:57
tags:
---

Cando me dispoño a facer arqueoloxía de autotest para a bitácora, atópome a seguinte situación:

- Repositorio de github: Último commit do 7 de Xuño do 2020
- Repositorio de @hopes
  - O estado de master figura sincronizado co último commit do github
  - Hai cambios fóra do _stage_, son axustes mínimos (2 liñas) nas expresións de `grep` para colorear _reports_
- Repositorio de @shitbook
  - O master está nun comit do 5 de Marzo do 2020
  - Hai un branch 'work-at-shitbook' cun commit extra do 1 de Maio do 2020 que reza `save urgently work at shitbook`
  - Non hai cambios fóra do _stage_
- Repositorio **bare** de @shitbook
  - O master está nun commit do 5 de Marzo de 2020 (o mesmo que o outro repositorio)

Comprobo que repositorio de @hopes ten o branch 'work-at-shitbook' co mesmo commit, polo que parece seguro que o repositorio de @hopes é o máis actualizado.

