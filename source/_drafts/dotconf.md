---
title: dotconf
date: 2020-07-12 11:00
tags:
---

Dende que comezamos a traballar diariamente na oficina en Setembro deste 2020, boto en falta maior facilidade para compartir as configuracións e customizacións que emprego habitualmente.

[dotfiles]: https://github.com/lorenzogrv/dotfiles

A última vez que me pasou isto, atopeime con que a miña primeira aproximación á xestión destes ficheiros - que no seu día chamei `[dotfiles]` - resultou obsoleta e ineficiente, e por iso decidín revisionala baixo o nome de **dotconf**.

Pero antes de entrar en detalles sobre **dotconf**, vou falar un pouco do seu precursor [dotfiles].

## dotfiles

[wiki-dotfiles]: https://wiki.archlinux.org/index.php/Dotfiles

Este proxecto foi dalgunha maneira o meu _bautismo de mar_ no mundo das [dotfiles](wiki-dotfiles).

Daquela xa trasteaba basante con [vim](https://www.vim.org/) e o terminal en xeral, e á vez que busquei a forma de compartir configuracións entre dispositivos aprendín un pouco sobre ficheiros básicos como os _profiles_ e algún dos sistemas de xestión de plugins de `vim`.

[bitácora]: /bitacora

Quixen recuperar do historial do repositorio as datas en que desenvolvín [dotfiles] e acabei por comezar a [bitácora], e inda que esa é outra historia o certo é que a [bitácora] comeza rememorando os meus propios pasos no histórico deste proxecto, anotando as datas para consultalas cando decida desenvolver a [Bio](/bio):

- Primeiro tramo, do 22 ao 26 de Maio do 2014
- Segundo tramo, do 11 ao 18 de Agosto do 2014
- Terceiro tramo, 8 de Xaneiro do 2015
- Cuarto tramo, do 20 ao 23 de Maio do 2015
- Último apunte, 2 de Xuño do 2015

[autopsia]: /2020/12/06/Autopsia/

Este repositorio nin sequera está clonado en @hopes, e tendo dúbidas de si existirá en @shitbook puido comigo a curiosidade e acabei facéndolle a [autopsia].

Despois comprobei que [dotfiles] morreu tanto atrás que nin sequera está na partición onde teño os repositorios.

## dotconf

Deixando aparte a historia dos [dotfiles], volvo ao fío de [dotconf]. Realmente [dotfiles] non ten relación directa con dotconf, pode considerarse coma un traballo de investigación previo.

[nomes propios]: /bitacora

Pode resultar obvio á vista dos [nomes propios] que figuran na [bitácora] que á hora de escribir por primeira vez sobre un proxecto escollese un dos do primeiro grupo.

[NIH]: https://wiki.c2.com/?NotInventedHere

O non tan obvio é que sexa un proxecto onde acadei unha pequena victoria contra o **síndrome do [NIH] (_Not Invented Here_)**, na eterna batalla que libro a diario contra el.

---

Ao volver ao repositorio, chámame a atención que está a empregar a api 'argv' de **bashido**, que non lembraba tan elaborada como para poder depender dela. Grata sorpresa :D

## bashido

Bashido é un xogo de palabras, _The path of Bash' spirit_, de maneira semellante a _Bushido_. Este proxecto nace dentro do monolito _iai_ e co tempo optei por independizalo, pero o certo é que aínda non cheguei a facelo por completo. Na práctica nin sequera ten repositorio propio na nube, e defeito en @hopes sigo a usar a versión que integra _iai_. Este é un dos repositorios que só estaba no disco de @shitbook, e vou aproveitar para anotar datas do histórico.

O certo é que cando _independicei_ bashido tentei extraer a información do histórico do repositorio de _iai_, pero hai un montón de commits nel que non pertencen a bashido.

Por fortuna `git` é máis listo ca min porque está ben feito, e quedou un primeiro commit antes do `git-subtree add` que marca o momento exacto en que fixen a extracción:

> **Primeiro commit de [bashido], 18 de Outubro de 2018**

Invirto algo de tempo en bucear para anotar as datas relevantes e dalgunha forma relacionadas con [bashido] a partir dese momento. Non resulta doado dado que cando creei este repositorio en base ao histórico do de `iai` realicei dous `git subtree add` diferenciados para os tests e a implementación, e por este motivo o histórico deste repositorio vai e ven no tempo varias veces.

---

A inmersión no histórico acaba nunha cantidade importante de anotacións na [bitácora], e como non podía ser doutra maneira tamén boto un ollo ao código.

As primeiras impresións son extrañas. Observo nun test un remix de apis de autotest, donde se usa tanto a primeira api (via eval) como o layer C da nova.

:omg:
