---
title: Bitácora
date: 2020-12-06 14:34:29
---

Xa escribín demasiado código fonte que non quero deixar no olvido.

Conforme ano tras ano fun aprendendo e producindo fontes que podo considerar
dunha mínima calidade, a carencia de coñecemento en métodos organizativos e
malos hábitos - como a tendencia a monolitos - provocaron que a gran maioría das
fontes que desenvolvín anden perdidas e misturadas en repositorios con demasiada
basura, en distintos dispositivos, en distintos discos, ...

> O caos é a orde natural das cousas

E dentro dese caos que me move acabei facendo arqueoloxía sobre os meus propios
pasos como desenvolvedor de software autodidacta, dando lugar a esta Bitácora.

---

## Nomes propios

Isto é unha especie de [índice de proxectos](./vault/).

> Cando un nome propio estea marcado, significa que fixen arqueoloxía no
> histórico do seu repositorio.

O primeiro grupo son proxectos que me importan máis e considero importantes para
min. Ademáis creo firmemente que o nome de cada un deles _está ben posto_ dado
que ten significado propio, e mesmo polas letras e conceptos que conforman cada
nome.

- `[x]` [autotest]
- `[x]` [bashido]
- `[ ]` [dotconf]
- `[ ]` [iai]

[autotest]: /ainda-non-existe
[bashido]: /ainda-non-existe
[dotconf]: /ainda-non-existe
[iai]: /ainda-non-existe

O segundo grupo son exclusivamente de paquetes para reuse de código con
repositorio propio no [github de iaigz](https://github.com/iaigz). A maioría
deles xurdiron moitísimo antes que o seu repositorio.

[util-ansi]: /bitacora/paquetes
[util-callsite]: /bitacora/paquetes
[core-log]: /bitacora/paquetes

- `[x]` [util-callsite]
- `[x]` [util-ansi]
- `[ ]` core-service
- `[ ]` tool-sources
- `[ ]` stream-keyboard
- `[ ]` stream-io
- `[x]` [core-log]
- `[ ]` core-section
- `[ ]` core-answer
- `[ ]` wui

O terceiro grupo son proxectos moi relacionados con investigación e
autoaprendizaxe, moitos deles con vida breve ou dalgún xeito abandonados pola
causa que for.

[dotfiles]: /non-existe

[iai-oop]: /paquetes
[iai-test]: /paquetes
[iai-is]: /paquetes

[iai-ide]: /prototipos

- `[ ]` practical-inheritance
- `[x]` [iai-oop]
- `[ ]` iai-schema
- `[x]` [dotfiles]
- `[ ]` iai-test
- `[x]` [iai-is]
- `[ ]` lorenzogrv.com
- `[x]` [iai-ide]
- `[ ]` iai-flow
- `[ ]` iai-loader
- `[ ]` iai-util
- `[ ]` good-practices
- `[ ]` lemmy-iai
- `[ ]` iai-middleware
- `[ ]` iai-component
- `[ ]` tait
- `[ ]` pingpong

Outra cousa que teño pendente é [revisar o resumen de actividade do meu
github](https://github.com/lorenzogrv?tab=overview&from=2012-10-01&to=2012-10-31)

---

## Arqueoloxía

Esta sección é unha sucesión de anotacións temporais en base á revisión de
commits de distintos repositorios, que dalgún xeito agardo me sirva para facer
autoanálise do camiño que levo recorrido como desenvolvedor de software
autodidacta.

> O obxectivo é construír unha liña temporal única, que se poda ler coma un
> diario

Isto é algo que leva tempo dada a cantidade de repositorios e commits, e os
pasos que completei ata o de agora son:

1. Apuntes temporais sobre [dotfiles], basándome no histórico do repositorio de
   github. 6 de Decembro do 2020
2. Apuntes do repositorio de [bashido] que ficaba no disco de @shitbook. 632
   commits revisados o 7 de Decembro do 2020
3. Apuntes do repositorio de [autotest] en @hopes. 662 commits revisados o 7 de
   Decembro do 2020
4. 73 commits dos repositorio de [iai-oop], 1 de [util-callsite],
   3 de [core-log], 2 de [util-ansi], 14 de [iai-test], 13 de [iai-is]
   Revisados o 16 de Setembro do 2022
5. 1 commit do repositorio de [iai-ide]. Atopeino por casualidade facendo outra
   cousa o 18 de Setembro do 2022, levei unha grata sorpresa ao lembrar qué
   facía por aquela. Volverá a integrarse no codebase de iai.

Conforme vou engadindo pasos á lista anterior e medra o número de apuntes, vou
refactorizando para reconstruír a liña temporal.

> Polo momento ten demasiado bruto, e fáltalle moito traballo aínda.

### Ano 2012

- Outubro 2012
  - 3
    - **Día en que creo a miña conta en github**. Nun inicio apodeina
      `laconbass`. Ha día de hoxe é [@lorenzogrv](https://github.com/lorenzogrv)

### Ano 2013

- Abril 2013
  - 23
    - **data en que creo o repositorio de [iai]**
    - Figuran commits (entre eles, o inicial) no github de [iai-oop]
  - 24
    - Atopo un commit no histórico de [bashido] que realmente ten que ver con
      [iai], e reza 'Prepare development start'
    - Version 0.0.31 no histórico de [iai-oop]
- Maio 2013
  - 8
    - Figuran múltiples commits no github de [iai-oop], que comezan con
      "0.0.4-beta". Falan de tests :D
  - 13
    - _evolving the iai oop concept to the iai oop compendium_ no github de
      [iai-oop]
  - 15 a 17: Atopo commits no histórico de [iai-oop] que falan sobre Composites
    e Deferred, FlowError. O 17 unha mensaxe reza _Worked hard on deferred and
    testing it. It seems pretty stable_
  - 19
    - "More tests and bugfixes" no repo de [iai-oop]
  - 20: Atopo un commit no histórico de [autotest] que reza `added iai-modules
    to gitignore`
- **Outubro 2013**
  - **10**
    - Atopo varios commits no histórico de [autotest] onde engado `iai-oop` e
      `iai-util` como submódulos ao repositorio de [iai]
    - Atopo un commit no repo de [iai-oop] que fala sobre a web de curricán,
      e algo dun _cleanup_
    - Commit inicial no repositorio de [iai-is]
  - 11
    - Versión 1.0.0 rc de [iai-is]
  - 15
    - Múltiples commits no repo de [iai-oop], incluídas dependencias e licencia
    - Commit inicial no repo de [iai-test], incluída licencia.
      "7 tests passing, 4 pending". Parece que ía en boa dirección
    - Engado tests e actualizo a licenza e o readme en [iai-is]
  - 16
    - **Working on practical inheritance paper** no repo de [iai-oop]. Múltiples
      commits, entre eles fálase de practical-inheritance
  - 17
    - 3 commits en [iai-oop], fálase de OopStandardApi e a independencia de
      "practical inheritance pattern"
    - Engado `is.Module` a [iai-is]
  - 18
    - un commit en [iai-oop] fala de cambios en [iai-test]
    - No repo de [iai-test] fitura que engadin `test.buider` e tests, hai un
      merge commit tamén
  - 22
    - un par de commits en [iai-oop] sobre `#accessor`
- Novembro 2013
  - 18
    - Hai un commit cun cambio "chorras" no repo de [iai-test]
  - 29
    - Engado un feature para xerar _tests cases_ basado en regex a [iai-test],
      incluíndo tests
    - Engado `is-literal` a [iai-is]
- Decembro 2013
  - 17
    - Modificacions practical-inheritance no repo de [iai-oop]
  - 18
    - Engado tooling para **code coverage** usando `istanbul` no repo de
      [iai-oop] e tamén no repo de [iai-is]
    - No repo de [iai-is] engado un feature "quick access" e o commit reza
      "prepare npm release"
  - 19
    - `#internal` + "prepare npm release" en [iai-oop]
    - `is.Null, isUndefined, is.Empty, is.Bool` + npm release en [iai-is]
  - 21
    - "experimental opp.callable api" en [iai-oop]
  - 23
    - Engado tooling para xenerar documentación usando doxit ao repo de
      [iai-oop], e parece que a desplego en gh-pages

### Ano 2014

- Febreiro 2014
  - 21
    - "Add port to iai-flow" no repo de [iai-oop]
- Marzo 2014
  - 10
    - Arranxo un bug con `is.Absolute` no repo de [`iai-is]
- Maio 2014
  - 22: Actualizo practical-inheritance no repo de [iai-oop]
  - 22 ao 26: Primeiro tramo do desenvolvemento de [dotfiles]
- Agosto 2014
  - 11 ao 18: Segundo tramo do desenvolvemento de [dotfiles]
  - 19: **Único commit no repositorio de [iai-ide]**
- Decembro 2014
  - 23
    - Engado un fetaure `test.exportsSomething` a [iai-test].
      **É o último commit que figura no repositorio**
    - Engado feature `is.EmptyObject` a [iai-is].
      **É o último commit que figura no repositorio**
  - 24
    - Engado o wiki do repositorio de [iai] no github como submódulo do
      repositorio de [iai]
    - Atopo un commit no repositorio de [autotest] onde engado como submódulos
      ao repositorio de [iai] a `iai-is`, `iai-loader`, `iai-oop`, `iai-schema`,
      `iai-test` e `iai-util`
    - **Nesta época andaba aprendendo a usar os submódulos de `git`**, porque
      tamén figuran un par de commits deste día no repo de [autotest] sobre
      fixes en submódulos ^^U

### Ano 2015

- Xaneiro 2015
  - 8: Terceiro tramo do desenvolvemento de [dotfiles]
- Maio 2015
  - **6**
    - **Primeiro draft do que no futuro sería [core-log]**, incluído
      [tool-callsite] e [tool-sources]
    - Observo no respositorio de [autotest] o commmit do README de [iai] que
      aínda figura tal cual no seu repositorio en github
    - Observo no repositorio de [iai-oop] "2.0 draft"
  - 8
    - Observo un update de [iai-oop] nos repos de [iai] e [iai-oop] que fai
      referencia ao engadido de `#build feature`.
  - 11: `#provider` con tests :D e `#flag` en [iai-oop], tamén figura no
    repositorio de [iai]
  - 15
    - Avances na api do que no futuro sería [core-log]
  - 20 ao 23: Cuarto tramo do desenvolvemento de [dotfiles]
  - 23
    - Elimino o submodulo [iai-loader] do repositorio de [iai]
    - Melloras diversas no que sería [core-log]: multiline, `mute`, cores
    - Mención sobre melloras en `test-loop`, e renomeamento a `iai-test`
    - Mención a melloras en [iai-oop], en concreto a `#flag`. No repo de
      [iai-oop] figura que engadín tests
- Xuño 2015
  - 1
    - No histórico de [iai] figura a **creación da función read** (javascript,
      streams). Esa mesma que lle suxerín a isaacs XD, teño que recuperar aquel
      link
  - 2
    - **Último apunte no histórico de [dotfiles]**
    - Mención sobre actualizacións en [iai-schema] no histórico de [iai]
    - Mención sobre engadido de `#lazyload` a [iai-oop] no histórico de [iai].
      É o **último commit que figura no repositorio de [iai-oop]**
    - Traballo sobre a función read e tests relacionados no histórico de [iai]

### Ano 2016

- Xaneiro 2016
  - 26: Actualización do submódulo coa wiki (doc) no histórico de [iai]
  - 27: **[iai] `2016-01-27 17:31 - Refining the iai concept. Related to #8`**
- Febreiro 2016
  - 3: Actualizo un link ao _philosophy behind the iai term_ no histórico de
    [iai]. Estes días debín andar escribindo
- Outubro 2016
  - 14: Creación do script principal de [iai], e polo tanto orixe de [bashido].
    O commit reza `sysadmin automation begins`
  - 25
    - **Orixe de `info()`, `warn()` e `fail()`** na interfaz CLI de [iai], que
      aparte de con [bashido] tamén ten moito que ver con [core-log]
    - Creación do script `iai-update-musescore` para @iaibox, no que vexo
      enlaces ás instruccións de compilado
- Novembro 2016
  - 17
    - Figura un commit que podo considerar a primeira categorización por apis de
      [bashido], que daquela metín nun ficheiro 'General bash helper funcions
      source file'
    - Aparición da función bash `iai()` no script principal de [iai]
    - Figura un commit que reza `learning git mergetool`
  - 22: Primeiro bloque de tests da lóxica implementada en [bashido]. Daquela o
    nome era 'bashlib' XD. Isto tamén ten relación con [autotest]
  - 25
    - Máis tests de [bashido], en particular a interfaz orixe de `autotest`
      (tested/teskip). Isto lémbrame que [autotest] naceu dentro de bashido
    - Hai un commit de [iai] relacionado con [autotest] en que se menciona o
      concepto **fail early**
- Decembro 2016
  - 1: Máis tests, en particular o mítico `diff_test`. Isto ten relación con
    [bashido] e [autotest] (mítico para min claro, XD)
  - 2
    - soporte para ler de `stind` para `diff_test`. Seguramente investigaba
      sobre redirección. Parece que esta tamén é a orixe do meu vicio polo
      _builtin_ `test`
    - Creación do script `iai-test`, que nunca evolucionou favorablemente
    - O caos muta en `dojo/`
    - Aparecen notas sobre a prematuridade en buscar unha estrutura de
      directorios, e o _commit msg_ é relativamente elocuente: `(...) while
      EVAding cowboy mode as no priority is clear now...`
    - Creación de `iai-find`
  - **16**:
    - **Bautizo a librería _bashlib_ como `bashido`** no package.json de [iai]
    - Investigo bastante sobre bash, á luz de varios commits
    - Aparece no dojo `gizmotractor` e as `node-install-notes-for-burzum`
    - Hai un merge de shitbook-tools onde aparece `autotest.bash`, ademáis de
      varias tools de bashido
    - Aparece `ascii-art` e notas sobre `terminix` (agora `tilix`)
    - Mención a `vim-hacking-reference`

### Ano 2017

- Xaneiro 2017
  - 27: **script `control`**, se non lembro mal unha investigación sobre
    servicios e init scripts. Creo que inda existe no dojo
- Marzo 2017
  - **9**:
    - **Hai un commit cuia mensaxe reza 'welcome bashido'**
    - aparece `dojo/chaos`
    - dentro da reestruturación do repositorio `iai`, **aparece o directorio `bashido`**
    - **Primeira aproximación á forma de importar bashido** `source $(bashido "api-a-importar")`
  - 10
    - [bashido] convírtese nun 'abc' de [iai]
  - 12: Avances de madurez na api de 'log' de [bashido]
  - 14
    - [iai] creación do directorio abc/command
    - **Decisión de favorecer o uso do singular sobre o plural nos directorios**
  - 21
    - Cleanup da estrutura interna de bashido
    - [iai] **Aparición do feature 'abstract'**
    - Primeiro commit que figura no `git subtree add` do repositorio de [autotest]
  - 22: [bashido] Melloras na api 'abc.ansi', fixes en abc.ansi e assert.lint
  - 23
    - No repositorio de [autotest], hai cambios no test 'check\_feature'
    - Hai un commit co profile de 'terminix' para _bed sessions_
    - O comando `iai-working` convírtese nun _builtin_ de [iai]
  - 24
    - Hai un par de commits con _tidy-ups_ do dojo de [iai], e un da raíz do
      repositorio de [iai] no que se move o 'db' a dentro do dojo
    - Creo un directorio 'todo' no 'db' do dojo de [iai] e anoto nun ficheiro
      **que debería sacar 'of web-based applications' da definición de [iai]**.
      Sorpréndeme ao velo.
    - Traballo no **Ficheiro `abstract/abstract.md`** de [iai]
    - Script para ver cores do terminal `iai-colors`. Lembro que o adaptei dun
      ficheiro de exemplo de Manjaro
  - 25
    - **preparación renomeamento iai-test → iai-check** no repositorio de [iai].
      Isto ten que ver co `check-command` de [autotest] e tamén relación con
      [bashido]
    - Menciono algo sobre un bug no parseo de argumentos do log
  - 25 e 26: [bashido] Perfeccionamento dos estilos ANSI do log
  - 26
    - **Mención a supernova e fighting frustration**. Hai unha serie de comits
      con fixes posteriores. Claramente perdín traballo por non ter no _stage_
      de git algunha cousa e haber executado algún comando que non debía. Teño a
      sensación de que sería algún `rm` XD
    - Un mensaxe menciona **`iai find in`**
    - Investigo sobre mecanismos para buscar 'todos' nos ficheiros usando `grep`
    - [autotest] Hai un apunte sobre `autotest allin`
    - Hai un rewrite potente de `iai-check`/`iai-test`
  - 27
    - Retocados os tests de bashido para adaptarse á convención de iai/abc
    - Avances en distintas apis de [bashido]
    - Empezo a ver protestas sobre a necesidade de manexar TODOS para parar o
      cowboy-coding (`iai-todo`). Este script marcou o deseño do sistema de
      axuda automática basada en comentarios da api de 'argv' de [bashido], e
      tamén das capas de [autotest].
    - **Elimínase `autotest` de bashido**
    - **Unha mensaxe reza 'bashido is now a project on its own'**
  - 31
    - categorización da api 'assert'
    - **Sintaxe de puntos para cargar sources**, `source $(bashido abc.log)`
- Maio 2017
  - 24
    - Mensaxe elocuente: `add crazy, stupid, and funny wololo` XD
- Xuño 2017
  - 6: Commando 'iai dropboxes'. Debeu ser cando creei a conta de dropbox para
    **iai studio**
- Xullo 2017
  - 4: reubicación de funcións de [bashido] nalgúns ficheiros
  - 5
    - Os commits inducen a pensar que volvín á actividade despois de facer un
      alto
    - Aparecen `iai-tree`, `iai-ls`, `iai-wanip` e `iai-lanip`. Claramente
      andaba perfeccionando as utilidades e customizacións que emprego a menudo
      no terminal. Isto ten que ver con [iai], pero tamén especialmente co
      abandono de [dotfiles] e a necesidade de [dotconf] ^^U
    - **A mensaxe dun commit reza que 'check' está roto**, e que resulta
      imposible entendelo despois duns meses de inactividade
    - Hai un merge dun branch chamado 'usermod' no repositorio de [iai]
  - 6
    - fixes en check/common e check/command
    - deprécianse algunhas funcións de assert/lint
    - hai un commit curioso sobre `example/data/bag`. Paxaradas de arquitectura
      e _naming_
    - Anoto que debería aprender a usar `rsync`
    - Fixes no script `iai-dropboxes`
    - Hai un fix de `iai check command` que asegura que naquel momento pasaban
      todos os tests de comandos de iai, e un inmediatamente posterior que di
      que non se estaban comprobando adecuadamente as dependencias XD
    - Actualizo o procedemento de _linting_ para comprobar a cabeceira, o
      contido e o final dos ficheiros; nese orde
    - En xeral traballo intensamente sobre iai-lint, ata unha mensaxe reza
      'preparando a _Separation of Concerns_ de iai-lint'
  - 7
    - Renoméase 'bashido/abc' a 'bashido/api' no repositorio de [iai], pero isto
      obviamente é de [bashido]
    - Aparece referencia explícita a bashido.require, e refactor ao respecto
  - 8
    - **Aprendín a usar a nivel avanzado o _parameter substitution_ de bash**
      para reemplazar o uso de `basename`
    - Aparecen referencias aos scripts específicos para `termux`
    - Unha mensaxe reza que bashido provoca un segfault en `termux`, e
      posteriormente evolucionan varios tests ata arranxar dito problema
    - Referencias a basic.math de [bashido]
    - Referencias a basic.path de [bashido]
    - Referencias a stdio.printy de [bashido]
    - Exemplos sobre check.command, algo que naceu en [bashido] pero ten máis
      que ver con [autotest]
  - 10
    - **Unha mensaxe reza 'Bashido is now stronger'**
    - Modifícase a forma en que se accede ao source code de [bashido]
    - Aparece a documentación de uso por primeira vez no script principal de
      [bashido], con 4 formas
- Setembro 2017
  - 7
    - Unha mensaxe protesta sobre a suciedade en cambios que quedaron sin
      commitear, sen posibilidade de determinar si estaban ben ou nou
    - O contido do commit contén esencialmente cambios en bashido
    - Deleteo o package.json de bashido, non queda claro porqué
    - Empezo a ver merges de 'develop', pode que sobre esta época coñecese e
      investigase sobre o _git branching model_
  - 9
    - Observo un fix crítico de 'exit instead return' no script principal de [bashido]
  - 13
    - Observo a creación do `READMY.md` no repositorio de [iai]
    - Dita creación pasa por unha queixa respecto a que bashido non é unha
      _feature_ de iai, senón unha librería, e que está almacenada nun lugar
      inadecuado
    - Elimínase o output de level verbose (VV) por defecto
  - 20: Observo o libro 'The Linux Command Line' na raíz de dojo/chaos
  - 22: Engado o _builtin_ `iai-status`
  - 23
    - Arranxo `iai-find` para sistemas basados en busybox
    - Investigo sobre as posibilidades de autenticación usando `ssh`, script `iai-ssh-auth`
  - 24: Creación de `iai-pkg` como mecanismo para simplificar a xestión de paquetes
  - 25: Traballo no `iai-pkg`, e **engado `apt moo` ao db/artii XD**
  - 27
    - observo unha queixa curiosa no READMY sobre o **chicken-and-egg problem**
    - utilidades para manjaro no `iai-pkg`
    - un mensaxe reza que **estaba a piques de wipear @ala-x**, e creei no
      dojo/chaos un `openwrt-gpio`
  - 28
    - observo un 'init iaido', e tamén outra queixa no READMY sobre que se supón
      debe ser unha librería
    - soporte para `iai-pkg` en entornos openwrt
    - soporte para `dropbear` (openwrt) en `iai-ssh-auth`
    - **decidín empregar o estilo javascript `standard` para ir máis rápido**
  - 29
    - soporte para comandos executables (doutros intérpretes) en [iai]
    - empezo a usar standard en vim vía ALE
  - 30
    - aparece [iai-oop] como submódulo
    - un commit reza 'first steps on iaido'
    - **Importo moito código antigo en `dojo/chaos/iaido`**, e en concreto mando
      `lib/agnostic/Conf` a `abc/lib/agnostic`.

      **Ogallá poda averiguar as datas de todo iso, porque ven TODO o vello**
      de node.js: async/{Heap,IOStream,Notifier,Queue,flow,sequence},
      content/{Item,Resource/routes} core/{mil movidas!}, data/{mil máis inc.
      validators}, db/{Base que basei na estrutura do Cogumelo, driver dummy,
      driver sqlite}, frontend, middleware, modules, utils...** Unha barbaridade
      de código, moito válido, desenvolto xa nin lembro cando. O commit é o
      af092690ad86b29299a872d2d7506ca6f13452a9
    - Melloras en `iai-lanip`
- Outubro 2017
  - 1
    - Importo os tests das antigas librerías de node.js a dojo/old-test
  - 24
    - Creación do comando de [iai] iai-net
  - 31
    - Creación do comando iai-mass-rename a partir do que había no meu
      repositorio privado de datos
- Novembro 2017
  - 2
    - Movo `iai-lanip` e `iai-wanip` a ficheiro de `iai-net`
    - Traballo nestas utilidades e aparece tamén `iai-net-gate`
    - Melloras en `iai-pkg` para entornos `opkg` (openwrt)
  - 3
    - Optimizacións de velocidade dalgúns subcomandos de `iai-net`
  - 6
    - Exemplo do programa de descompresión de ficheiros, baseado no
      `/etc/skel/.bashrc` de manjaro
  - 9
    - **Elimino o submódulo 'wiki'** según o histórico de [iai]
    - No histórico de [iai] indico que atopo traballo sin gardar en @hopes.
      Revisando observo que ten relación co **gui vía electron**,  aparte de
      outras cousas (commit b0b68f1122cb9975e18d272803d15cc983e55189). Máis
      adiante hai outro commit que reza `fix api/gui`
    - **Parece que este día empezo a traballar sobre _pingpong_**. Importo
      moitísima mierda de node\_modules omg!
    - No histórico de [iai] indico que elimino os submódulos [iai-oop] e
      [iai-schema]
    - Observo que inclúo `ultron` entre moita mierda nos node\_modules. Non
      lembro ben a utilidade que tiña
    - Hai un montón de commits e merges por mor da catástrofe de engadir
      node\_modules e andar cambiando de dispositivos ao traballar sobre
      pingpong
  - 10
    - Hai un commit no histórico de [iai] que reza `fix iaido (homenaxe andrade)`
  - 27
    - Vexo enlaces novos en dojo/notes/v-links.md`, entre eles referencias a
      libros sobre deseño
- Decembro 2017
  - 05
    - [iai] apunte sobre un fix para `iai pkg search` en entornos arch
      (`pacman`)

### Ano 2018

- Xaneiro 2018
  - 25
    - Creación do comando `iai-ssh-auth`
    - notas sobre como acceder a ficheiros nun dispositivo MTP (android)
  - 30
    - Soporte a `iai ssh-auth` para servidores obxectivo que usen dropbear (openwrt)
    - Notas en [iai] sobre adaptar o comando `iai-pkg` para android/termux
  - 31
    - Arranxo o que sería [core-log] para o estilo `standard`
    - Adapto a proba de concepto [pingpong] para clientes múltiples e
      dispositivos móviles
- Febreiro 2018
  - 1
    - **observo as primeiras referencias a [keyboard-stream]**
    - Mencións a melloras en `Router` e fixes no comando de descompresión
    - Traballo na función read, refactorizándoa para que empregue Streams
    - Traballo na lóxica de [keyboard-stream] e o conversor utf8keys
  - 2
    - Sigo fuchicando en [pingpong]
    - Comezos do comando [iai] `iai-package`
    - Mención á creación da ferramenta `packdata` no repositorio de [iai]
  - 5
    - Algo andaba a facer con openwrt no dojo. Mencións a `iai-net` no histórico
    - Mencións a fixes en `Job`
    - **Primeira mención que vexo nun commit msg sobre `browserify` :)**
  - 6
    - Landscape layout para [pingpong] Mención sobre que estaba traballando coa
      @shitblet
    - Reubicación de ficheiros e funcións no código do que era [iai-abc] (entre
      elas [tool-sources]
    - Reescritura do módulo principal de [iai-abc] para adaptalo ao estilo
      `standard`
  - 7
    - Parece que monto un cristo en [pingpong] e movo o 'backend logic' de lugar
    - Reescritura da api "path" de [iai-abc] para adaptala ao estilo `standard`
  - 15
    - Parece que traballo plenamente enfocado en [pingpong], investigo máis
      sobre `watchify`, websockets, e o stdio de `spawn`
    - Primeiro borrador de `View` para browser-side
    - Hai unha mención curiosa a iai-studio e á eliminación do `example`
      paxareiro de arquitectura para movelo a dito repositorio. Flipo XD
  - 24
    - Gardo un link misterioso sobre amplificadores rectificadores en v-links
    - Debía andar peleándome coa locura de ramas, porque importo `iai-net` do
      branch 'pingpong'
    - Mención a melloras en `iai pkg` e o engadido de
      `iai-pkg.pacman.fix-newer-than`
  - 25
    - Traballando en [pingpong], menciono que fago _decouple_ de `View` para
      frontend e backend e tamén vexo o **prototipo de widget de terminal web**
  - 27
    - Referencias a melloras en `iai-pkg`
    - Referencias a melloras en `View
  - **28**
    - **Referencias a que extraigo a lóxica de `View` para browser** ao
      directorio iai/api
    - Engado `iai-reader` e `iai-pager`
    - Importo o browserify-handbook no dojo
    - Actualizo os process bindings de [iai-abc]
    - **Actualizo o Log para que escriba todo en stderr**
- Marzo 2018
  - 1
    - observo referencias aos conceptos 'answer' e 'service'
    - actualización de `iai-ls` e `iai-tree` para direccionar a `iai-pager`
    - **Observo unha nota no READMY.md de [iai-abc] queixándome en galego da
      falta de diferencia clara entre 'api' e 'abc'**
    - Hai outra queixa no READMY de [iai-abc] en galego de que "read" debería
      estar en 'abc' e non en 'api'
    - **Rename de Server a Service**, a lóxica de websockets de [pingpong] pasa
      a Service
    - Mención sobre un intento de adaptar o antiquísimo plugin de jQuery
      iai-navigation (creo que iso o desenvolvín para a web de curricán)
    - **fix de [tool-sources] para casos en que existan dependencias circulares**
  - 2
    - Parece que estaba inseguro sobre algo chamado `View answer` (lol)
    - Mención a fixes en `iai-dependants`
    - mención a unha mellora en `iai-reader` sobre colorización e pipes
  - 3
    - Mención a un fix en [tool-sources]
    - Mencións a melloras en service.answer, Service, e #service (lol?). Mención
      a estandarización entre Service-backend e Service-browser e conversión a
      prototype
    - Mención a **decouple iai-api to its own package**
    - **Mención a renomeado de View a Section para evitar seguir a nomenclatura
      MVC**. Iso é a primeira mención que atopo ao nome de [core-section]
    - Refactor en [pingpong] para arranxar en base a todos estos cambios
  - 4
    - Adaptación da api de [core-log] para o browser. Notas sobre a necesidade
      de adaptar [util-callsite]
    - O paquete [iai-abc] convírtese en "lib" ... facepalm
    - Mencións a melloras en service e adaptacións diversas de utilidades para
      entornos browser
  - 5
    - **Mención a decouple de Stdin do widget de terminal, que a maiores renomeo
      a devtool**. Melloras na comunicación en tempo real vía websockets e fix
      de un bug en readkeys. TELA MARINERA ESTE COMMIT
    - **Movo [pingpong] ao directorio `app/`. O commit reza `forget multiple apps`**
    - Bump do número maior de versión de [iai-abc], por cambios _breaking_
  - 6
    - **Engado tests para cazar bugs de dependencias circulares en
      [tool-sources], e implemento fixes**
  - 7
    - **observo a primeira referencia a `gulp` no histórico de [iai]**
  - 9
    - Hai un fix de `iai-find` na construcción do argv
  - 14
    - **fighting insomnia abstract**
    - Refactor importante de [stream-keyboard]
    - **Primeiras referencias ao concepto 'content' como raíz de contidos (sitemap)**
- Maio 2018
  - 10
    - Comando `iai sync`
  - 13, 14, 22
    - Mención a `iai shitty auth`, fixes en iai-sync, creación de iai-net-ping,
      menciónase o draft de _is cloudy_ en `iai-net`
- Xuño 2018
  - Engándense as accións `upgrade` e `cleanup` á estratexia para `apt` de `iai-pkg`
- Agosto 2018
  - 16: Aparece un commit que reza `old work on iai application`. Claramente xa
    non me lembraba que iso era [pingpong]
  - 25
    - **Investigo sobre como streamear audio a través de ssh**, commit
     05d83a84d4eb97b0febfa56d0b23f172eb84ce4d
    - Investigo sobre a autenticación en Linux usando `su` e a suite `shadow`,
     menciono que debería investigar PAM. Isto desemboca en `iai-login`
  - 28
    - O día empeza con commits mois elocuentes sobre `Origins`, como `Now main
     entries ar '0' like Origins`. Isto é de [autotest] pero parece que entrei
     en barrena renomeando ficheiros index.js a 0.js...
    - Mencións a que [core-section] está vinculada con UI.
    - Convención de expandtab no tail dos ficheiros js
    - Extraigo os tests de [stream-keyboard] do gulpfile, e realizo refactor de
      dita utilidade
  - 29
    - Mencións a fixes no gulpfile
  - 30
    - Fago limpeza en varios sitios, e adapto algún ficheiro ao estilo `standard`
- Setembro 2018
  - 5
    - **parto o gulpfile en múltiples ficheiros**. Commit
      d4a3b9ee0db45aa8701efc1dd2db73b2e3f561ce en [iai]
  - 13
    - Primeiro script de `install` de [iai], bastante rudimentario. Adaptoo para
      termux e sistemas basados en busybox.
    - Adapto `iai-pkg` para entornos sin `sudo`, e doulle a habilidade de
      destruír paquetes en entornos `apt` (`apt purge`)
- Novembro 2018
  - 11: Melloras en `iai ls` e `iai net`
  - 12
    - Fixes para abc.ansi de [bashido] en terminais 'dumb'
    - Fixes varios que afectan a tests e saen no histórico de [autotest], que me
      fan pensar que andaba a traballar multi-device entre @ala-x, @mia, e
      @shitbook
  - 13
    - Avances en `iai-sync`
    - Fixes para [iai] en entornos openwrt
  - 23
    - Sigo a traballar en `iai-sync`
    - Cleanup post-upgrade para `iai pkg`
    - **test absurdo de opengl feito en C**

### Ano 2019

- Xaneiro 2019
  - 29: Fixes para `iai-pkg` en entornos pacman
  - 30
    - Fixes para ansi de [bashido] cando está deshabilidado
    - `iai-view` pasa a usar `vim`
    - **Aparición de `iai-watch` como builtin**
  - 31
    - Investigo bastante nos manpages de `tput`
    - Eliminación de secuencias ansi de [bashido] que a maioría de programas non
      entenden como cores
    - Fixes en log de [bashido] para formateo de múltiples valores
    - Traballo na tontería `iai-moods`
- Febreiro 2019
  - 1
    - Refactor do script principal de [iai], primeiro borrador de `iai render`.
      Figuran melloras en `iai status`, `iai watch` e `iai pager`. Semella que
      andaba peleándome con `urxvt`
    - **comezo do refactor de [autotest]** (primeiro borrador do test runner)
  - 2
    - Insercións no script principal de [autotest] entre elas axuda e cli de:
      `autotest`, `autotest checkup` e `autotest allin DIRECTORY`
    - No repo de [autotest] figuran fixes no exit status de `autotest report FILE`
  - 6
    - **Mención ao Autotest Protocol v1** no histórico de [autotest], pero non é
      a orixe
    - Melloras para configurar o nivel de verbosidade en `iai command`,
      habilidade `bail` para `iai test`
  - 13
    - Observo o movemento de todos os tests a `test/`. **Maldito o día**
  - 27
    - Habilidade `iai test list` para ese comando de [iai]
    - Cleanup dos tests de [autotest]
  - 28
    - **Varios commits no repositorio de [autotest] que semellan as primeira
      mención a fighting the cowboy**
    - Exemplos varios sobre o protocolo de [autotest] implementados puramente
      con `echo`
    - Elimínase o commando 'allin' de [autotest]
- Marzo 2019
  - 1
    - Varios commits no repositorio de [autotest] con máis exemplos e tests
      usando puramente `echo`. Os últimos falan de 'almost done'
  - 2
    - Un commit con múltiples ficheiros no repo de [autotest] reza `fighting the
      cowboy: chaos pre-antroido`
  - 4
    - Un commit no repo de [autotest] con cambios no script principal reza
      `fucking cowboy, go refactor tests`
  - 6
    - **Commit sustancial no repo de [autotest], onde me queixo de que non estou
      'fighting the cowboy' e anoto que debería testear `autotext`**
  - 12
    - Rework dos exemplos de [autotest], 'step1: test script' e 'step 2: asssert-correct-output-utility'
  - 13
    - Rework dos exemplos de [autotest] 'step 3: refactored OK examples'
  - 14
    - rework dos exemplos de [autotest] 'step 4: refactored FAIL examples'
    - melloras nos exemplos _hardcoded_ de [autotest]
    - **Engádese a utilidade check-command**
- Setembro 2019
  - 29
    - [bashido] variable de entorno para máxima verbosidade
    - [bashido] primeiro borrador da interfaz 'api.argv'
    - [bashido] actualización do comando root para podelo reusar
  - 30
    - [bashido] Melloras na rutina automática de axuda en api.argv
    - [bashido] Trazados condicionais na api de log
- Outubro 2019
  - 1: [bashido] limpeza na saída de log
  - 6
    - [bashido] fixes e melloras de estilo no log
    - Primeiro commit no repositorio de [autotest]
    - Commit no repositorio de [autotest] que reza `work over autotest that
      was @hopes without being commited`
    - **commits de `git subtree add` da creación do respositorio de [autotest]**
  - **7 a 11, 19 e 20**
    - **Split do script principal de [autotest] en múltiples ficheiros**:
      autotext, autotest, autofail, autoexec, autocode
    - Desenvolvemento principal de **check-command** en [autotest]
    - **Deseño da interfaz extensible para scripts** baseado en **É mellor usar
      as unidades para os casos que para a lóxica de tests**
    - **Deseño por capas de [autotest]**
    - **Desenvolvo a extratexia de parseo de argumentos `key=value` usando
      `declare`**. Teño que buscar o link en stackoverflow
  - 18
    - [bashido] Melloras na saída de axuda automática de api.argv
    - **commits de `git subtree add` da creación do respositorio de [bashido]**
    - [bashido] Extracción dos exemplos ao seu propio directorio

> **Creación do repositorio de [autotest], 6 de Outubro de 2019**
>
> **Creación do repositorio de [bashido], 18 de Outubro de 2019**

### Ano 2020

- Marzo 2020
  - 5
    - **Engado o feature `autogrep` a [autotest]**
- Abril 2020
  - 6
    - Único commit no repositorio de [util-callsite]
    - Primeiro commit no repositorio de [core-log]
    - Primeiro commit "Version 1" no repositorio de [util-ansi]
- Maio 2020
  - 1
    - Anotación `save urgently work at shitbook` no repositorio de [autotest].
      Creo que esta é a data en que amosa os síntomas de EOL que se mencionan na
      [autopsia]
- Xuño 2020
  - 7
    - **Engado o feature `autotest grep color` a [autotest]**
  - 11
    - Desactivo _process bindings_ no github de [core-log]
- Novembro 2020
  - 25
    - **Creación deste blog**. Isto realmente foi un exercicio para investigar
      sobre o CI/CD de GitLab, pero rematou sendo un proxecto en sí mesmo que me
      fai certa ilusión.
  - 26
    - Investigo máis sobre `hexo`, o motor de _blogging_ basado en estáticos que
      emprego neste blog, e creo a páxina da [bio], pero sin contido de interés.
  - 28
    - Escribo [þreludio](/2020/11/27/preludio/) como recordatorio dun futuro
      post sobre a teoría do C14
- Decembro 2020
  - 2
    - Escribo [Åctitude](/2020/12/02/Actitude/) e corrixo un par de problemas no
      blog
  - 4 e 5
    - Escribo [ɠurus](/2020/12/04/gurus/) e
      [ʘʜɱ](http://localhost:4000/2020/12/05/ohmn/), e tuneo mínimamente o blog.
  - 6
    - **Creación da presente [bitácora]**, que nun inicio era só unha lista
      simple de proxectos en base ao que atopei no [meu
      github](https://github.com/lorenzogrv), e no _vault_ de @hopes
    - Realizo a **[Ăutopsia](/2020/12/06/Autopsia/) de @shitbook** e escribo
      sobre ela.
    - Engadinlle soporte a blog para emojis, usando `hexo-filter-github-emojis`
  - 7
    - Doulle forma a esta [bitácora], nas dúas seccións que ten agora. No futuro
      terei que dividila en ficheiros porque xa ten sobre 600 liñas, e as que
      lle faltan... Farei mención a ela na páxina da [bio].
    - Arqueoloxía do histórico do repositorio de [bashido] do disco de @shitbook
    - Arqueoloxía do histórico do repositorio de [autotest] en @hopes
    - Borradores dalgún futuro post sobre estes proxectos que estou a revisar
  - 8
    - Preparación para reinstall de @garda-do-cabo. Estiven a piques de
      atacarlle, pero ao final deixeino parar para probar ben distintas distros.
      O plan é usar mint (cinnamon ou mate), ou manjaro con kde. Seguramente
      tire por mint.
  - 9
    - Despois dunhas probas, decidinme por mint cinnamon dado que vai ser máis
      simple de usar para os _viejos_, e reinstalei @garda-do-cabo. Todo ben,
      menos as peleas coa resolución e a _fucking_ impresora, que quedou sin
      acabar.

### Ano 2021

- Xaneiro 2021
  - 20
    - Fíx "crítico" no github de [util-ansi] - último commit
  - 20
    - Desactivo _custom warning wrapper_ no github de [core-log] - último commit

### Ano 2022

- Setembro 2022
  - 8
    - Escribo [∮rases](/2022/09/08)
    - Decido publicar [@iaigz/gitlab-ci](https://gitlab.com/iaigz/gitlab-ci)
  - 14
    - Por motivos persois decido retomar a costume de escribir como hábito, e
      ao escribir un [artigo sobre iai](/2022/09/15/iai/) atópome algo curioso
  - 16
    - Decido arquivar os repositorio de github de [util-callsite], [core-log],
      [util-ansi], [iai-oop], [iai-test]
    - Ao recuperar o código de [iai-test] e [iai-is] en github para resucitar
      o repo de [iai], sorpréndome gratamente de que deixase tests ben feitos,
      e mesmo informes de cobertura, que seguen a funcionar hoxe
