---
title: Paquetes
---

Isto é un índice provisional onde manteño enlaces sobre os paquetes que
desenvolvín

- util-ansi
  - <https://github.com/iaigz/util-ansi> (Arquivado)
  - `https://gitlab.com/iaigz/util-ansi` (Eliminado)
- util-callsite
  - <https://github.com/iaigz/util-callsite> (Arquivado)
- core-log
  - <https://github.com/iaigz/core-log> (Arquivado)
  - `https://gitlab.com/iaigz/core-log` (Eliminado)
- iai-oop
  - <https://github.com/lorenzogrv/iai-oop/> (Arquivado)
- iai-test
  - <https://github.com/lorenzogrv/iai-test> (Arquivado)
- iai-is
  - <https://github.com/lorenzogrv/iai-is> (Arquivado)
