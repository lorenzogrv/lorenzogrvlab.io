---
title: 🂭🂱boom
date: 2021-03-20 10:35:26
tags: Filoladas
---

Por algún motivo que non alcanzo a comprender de todo, é coma un diaño durmido
que esperta por veces.  Orgullo? Ego?

:thinking:

Non o sei, o que está claro e que debo buscar o _hack_ que me permita afrontalo
sen estoupar de tal forma. Despois síntome coma a merda, ergo non ten xeito nin
maneira.

Quizás a paciencia sexa o escudo contra min mesmo en situacións así, canto menos
ata que identifique qué tara é a que me provoca semellante paxarada.

Keep calm
