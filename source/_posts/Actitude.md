---
title: Åctitude
date: 2020-12-02 23:45:32
tags: Filoladas
---

A **actitude** é a forma en que se decide afrontar unha sensación.

Se non escribín nestes últimos días foi por falta de actitude, non de ánimo, e
eu decido a actitude coa que afronto o impasible - e por veces cruel - avance do
tempo.

Co novo setup vía `termux` en _miae_, non teño excusa.

L.
