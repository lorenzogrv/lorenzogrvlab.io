---
title: ȡọjo
date: 2022-09-19 09:10:01
tags: Informática
---

Onte arranquei un proxecto novo, que bauticei como **dojo**. Levaba uns días
medio obsesionándome con _iai_, e apetéceme cambiar de aires para non tolear.

![Dojo](/images/Dojo.jpg)

A historia vai de ensinar e aprender, e a tecnoloxía detráis de él quizá acabe
dando soporte técnico á [abc bible](https://gitlab.com/lorenzogrv/abc-bible),
que polo momento manteño en privado pero máis pronto que tarde verá a luz.

Ilusióname moito a posibilidade de formar a alguén no sentido de ofrecerlle
coñecemento para capacitar, para darlle ferramentas e mellorar o seu _know-how_
dixital.

Vexo un camiño interesante ahí, teño curiosidade por experimentar a onde pode
levar iso.

Bo día!

:sunny:

L.
