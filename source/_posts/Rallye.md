---
title: Ɍallye
date: 2020-12-12 08:08:45
tags: Crónica
---

A que segue é a crónica do vivido no [II Rallye da Mariña Lucense](http://www.peachaparacing.es/2020/12/guia-tramos-horarios-y-onboards-rallye.html), escrita por un profano total na materia pero ó que lle sabe como as amoras sentir o barullo dos motores dando gas polas pistas.

A maior parte das notas foron tomadas en tempo real, conforme foi avanzado o día, facendo énfase sobre todo en anotar a forma en que os coches pasaron polos lugares onde eleximos ver as pasadas.

---

> 08:20 GMT +1

:coffee:

Mentras lle aplico ao brebaxe clásico da mañá apertrecho o móvil coa [lista de inscritos](http://www.fga.es/wp-content/uploads/2020/12/LISTA-DE-INSCRITOS-MARI%C3%91A.pdf) e os [horarios dos tramos](https://1.bp.blogspot.com/-D2nuzqXl4jk/X851aEtvAlI/AAAAAAAAQrc/5LLdcr2CJ3E8KArSLkT6yrlcYqaivjY6gCLcBGAsYHQ/s16000/Tabla%2Bcon%2Bfirma%2By%2Blienzo.png). Neste momento non o sabía, pero logo enteraríame de que este rallye é de tipo _sprint_, porque os tramos son cortos e bastante rápidos. No noso caso, como nos cae máis cerca, imos ver como pasan no tramo de Cervo, inda que non é o máis complexo dos 3 tramos. Este artista chamoulle onte TC1 ó TC3 me parece...

:joy:

Ás 8.30 da sinais por _guasá_ o especialista, parece que espertou a _bella durmiente_. A ver si vamos á horquilla esa logo ou que, o ano pasado non acertamos ben co sitio e este ten mellor pinta. Según chega xa viña coa transmisión en directo posta no móvil, apretámoslle outro café e arrancamos na furgoneta.

> 09.30h GMT +1

Chegamos ó lugar indicado e hai un ambientazo que nin dios! Según aparcamos xa sentimos as sirenas do _reconocimiento_, e cagando virutas po monte jejeje, inda que teóricamente ata as 9.56 non sale o primeiro. Según chegamos había un rapaz co _chalequillo_ que nos escaneou os códigos QR polo rollo da covid, todo moi correcto.

Ás 9.38 volve pasar a asistencia, e ás 9.46 as últimas _sirenas_. Con cada pasada nótase que a xente se vai animando e están os ánimos _exacerbados_.

![Spot 1 · Horquilla Senra](/images/Rally-spot-1.jpg)

> 09.55h GMT +1

Síntese o bruar monte abaixo da primeira saída, e nótase no ambiente. Non tarda moito en aparecer Senra dando gas e boa conta da lama do interior da curva.

O ruso pasa segundo e deuna perfecta e moi ó animal, vaia becerro.

O terceiro viña mais achicao pero deixounos os tímpanos po arrastre jeje.

O cuarto nin tan mal, unha boa patinada. Xa empezan a facer boquete na esquina e a untar de lama o asfalto.

O quinto entrou algo pasado

O sexto deu un bon espectáculo inda que tampouco deu a curva do xeito máis rápido

Decia do anterior pero o sétimo si que derrapou guay

O oitavo parecido, pero xa se nota que os motores non arrean o mismo 

O noveno pasa e moi pegado a él xa se sinte o décimo, cortando muy ó becerro. Débenlle ir apretando

No seguinte empezo a fixarme nos números, é o nº 12 e pasa no sitio do 11; e detrais del pasou o nº 15. A ver quen segue a lista agora XD

Pasou o 16, este xa é doutra categoría. A ver si sigue en orden. Correcto, pasou o 17.

O número 18 armou un bo especáculo pero porque casi tira para burela jajajajaja. Seica non ía compenetrado coa copiloto.

O 19 dos berberecho pasou a trompicóns, arrastrando o cu da lancha esa jejeje

Pasou o 20, e o 21 detrais. casi me perdo na lista xa con tanto apellido jajaja, algun número se me escapa.

O 22 ven no seu sitio pero achicado e con pouco peito. Dice o especialista que debe estar algo queimado.

O 23 parecia que se pasaba pero non, _sin pena nin gloria_.

O seguinte debe vir cerca, que xa o sinto. Ahí pasa, facendo acrobacias de xeito algo torpe XD. Era o 26, o 25 debeu quedar facendo etcéteras na cuneta jajaja, vaia nome lle puxeron á escuderia.

O seguinte viña de paseo, non fichei ben o número pero creo que era o 26

Síntese un becerro ó lonxe, e ahí ben o 28. Este inda lle iba dando brea.

O 29 pasou tamén _"correctamente"_. Este pensou que era un 106 ó lonxe XD. Ahora teóricamente veñen 4 seguidos, moito tardan XD.

O 30 pasou labrando no rego, xa esta fina a curva de terra.

O 31 tarda en pasar, deu a curva coma si fora un trailer XD. Xa chispea, a ver si aguanta si caer ó bruto. Certo é que non sonan mal os VTS estos.

O 32 pasou asegurando, clavou freno e levantou datrais á dereita. Tardou abondo en pasar, mentras tanto  chove finiño xa.

O seguinte pega un bon petardazo entrando, e deuna bastante ben. Pena co que me sabe a min o barullo que non arreara tanto ese motor. Non fichei ben o numero.

Pasou o 34, o anterior debía ser o 33 logo. Agora xa veñen moi espaciados, empeza a rallar a espera. Estase a cerrar a néboa ás nosas costas.

Pasa o 37 cheirando a chamusquina. Debe haber baixas xa XD  

Pasou o 24 agora. Debeu ter algunha historia para vir tan descolocado. Chove algo mais gordecho, pero auga fina igual.

O 39 é o que segue, e deuna guay. Inda molaba o color, lembroume ao ibiza amarillo do mítico xogo aquel jeje.

O suzuki número 40 xa viña coas luces postas, parecía que iba pasado pero pegou unha patinada épica para enfilar e levantou o ánimo da tropa jejeje

O 42 viña relativamente cerca. Este sonaba moi becerro, di o especialista que debía fallar un cilindro ou algo.

O corsita dos tino racing a pouco mais e non sube a costa, vai a _rastras_ coma quen di.

O 44 viña moi pegado a el, ben sei que vai ter adiantarlle.

Despois pasa o 46, outra lancha que lle da unha boa limpieza á curva.

O mishubishi 47 deu un derrape con moita elegancia jejeje, como cubicado a compás. Bo espectáculo.

Parece que volven vir pegados, o 49 xa pasa sin darme casi tempo a escribir e ainda molou abondo como a pasou, atravesando largo.

O 51 sonaba guay, pero deu a curva coma un _pringao_. Ou iba achicado ou algo, porque inscribirse nun rally pa eso xa ves tu. Chívame este que un tal brais (o nº 60, cun 106) é de por acó

O 52 debeu levar un susto xa, pasou daquela maneira e traía felga na defensa de diante. Debeu andar rozando jejeje

Pasa o 55 logo dun anaco, derrapou en plan _yeah!_ pero levaba unhas llamas naranjas horteras

0 capó do 56 rezaba concello de xove jejeje, pasou tan pegado ao anterior que non me deu tempo casi a escribir

Xa sinto o seguinte, non sei que armou antes de entrar o renault nº 57 de colores. Deuna _ni fu ni fa_, e ó subir sentiuse como frenaba bastante, deben ir con respeto jeje 

nº 56 despois muy pegado, sonaba guay. Veñe moi cerca uns doutros agora.

O escort mk1 pasou dando un bo _show_, mola a saco ese coche.

O 50 ven despois e xa lle falta máis de media defensa datrais e un cacho de maletero, e o cacho que lle queda da defensa á esquerda xa ía colgando.

O Brais ven dando espectáculo tamen, ata fixo ademán de parar a saludar e todo jeje. Veñen apretaos e da pouco tempo a escribir, xa sinto o seguinte.

Era o 62 pasou moi fulero o cabrón XD

O 63? pasa despois, mentras lle presto o mechero a un máquina destes que viñeron todos preparados á praia jeje

O 64 pasa como é debido, dando espectáculo. Levaba colgando por debaixo e contra atrais a chapa, non creo que chegue coa peza á noite.

O 66 pasa despois cunha arrastrada criminal que apouco máis chega á tagea do lado de fóra XD. Así é a gracia. Xa ven outro

O 67 pasa mentras abre o sol de volta, e pega unha sachada ao entrar que debeu quedar fino por baixo. Xa sinto o seguinte, logo non da tempo a escribir XD

68, entrou guay pero esvarou de diante e tuvo que correxir, casi fai a daquel do ano pasado que tiveron que empuxar, pero maniobrou ben e tirou sen máis problemas.

O 69 ni fu ni fa, agora veñen cerca outra vez, así presta mais


O que reza "racing" na luna traseira (xara?) casi deixa a defensa de diante. Sonaba a cafetera

O 71 deuna perfecto pos _frikis sin puta idea_ coma min. Desde logo non é a forma rápida, pero presta máis velo asi.

O 72 xa entrou preparando o show tamen. Viña moi cerca e abreuse adrede para tensarlle, un pouco mais e da o trompo enteiro jejeje

O corsa do nº 73 foi épico! Ao entrar labrando abreuselle o maletero, e mentras subía pola costa ía abrindose de todo e escoitábase á perfección o son dos bombines empuxando o portón jajaja.


O pandita do nº 74 non lle fai nin sombra. Pasou máis achicado que outra cousa, e sonaba moi fulero. Teóricamente falta un, ahí ven xa.

Era o panda do nº 75, non molou moito pero sonaba mellor que o anterior.

>  11.30h GMT +1

Pararece que xa acabou. A ver se imos ao bar e poño en orden as notas. O especialista escapou cando aínda faltaban varios coches por pasar porque tiña un compromiso, pero no lugar tamén estaba _barba e melenas_ e acércame el. Así puiden quedar facendo a crónica, inda que quedei sen o cámara.

:joy_cat:

Ás 11:40h inda era cedo para o lugar especial habitual, pero a Pousada non falla e despois dunha voltiña céibanme alí. Toca _avituallarse_ mentras espero a que de sinais outra vez o especialista. A tapiña de ovos recheos sóubome a gloria.

:beer:

---

Preparando a publicación días despois, descubriría que [só marcaron tempo 58 coches neste primeiro tramo de Cervo](http://temposfga.es/wp-content/uploads/11_C-1-1.pdf), dos [72 que autorizaron a tomar a saída](http://temposfga.es/wp-content/uploads/6_AUTORIZADOS-1.pdf), pese a ser 75 inscritos. Foron quedando menos dos [67 no TC1 de O Vicedo](http://temposfga.es/wp-content/uploads/7_A-1-1.pdf), e [62 do TC2 de Xove](http://temposfga.es/wp-content/uploads/9_B-1-1.pdf). Nesta primeira volta, 14 coches quedaron fóra.

---

>  12:01h GMT +1

Recibo sinais _guasaperas_ do especialista, e non tarda moito en chegar á Pousada. Dalí a un cacho paramos a repostar de verdade no ultramarinos, pan e fiambre non poden faltar na bolsa. Non nos apuramos moito, falta abondo para o TC6 e non temos claro onde ir velos.

Feita a repostaxe no ultramarinos, imos ó lugar máis especial da volta tomar a segunda. Alí entérome de que anularon un dos primeiros tramos, porque volcou un coche. Pouco despois soubemos que foi o TC2 (Xove), e varios días despois puiden comprobar que o último tempo foi o do control 34.

Tamén aquí me entero do recente cambio de Senra a Michelín, da man de dous entendidos que estaban no lugar. Tamén comentaban que a vantaxe en tempo que acumulaba o ruso tiña en parte que ver con que as gomas de Pirelli van mellor en mollado.

---

> 13:35h GMT +1

Deunos por investigar un sitio para ver o TC6, un cacho mais arriba da curva do ano pasado. Arrancamos para aló recén pasada a primeira sirena, colleunos a segunda de camiño, e segundo acampamos pasa a terceira. O especialista fíxome acabar a milnove ás carreiras, pero sitio ten pintaza. Ao final _barba e melenas & co._ animáronse a vir aquí tamén, de feito non recordo ben pero creo que foi suxerencia del pola mañá.

![Spot 2 · Panorámica](/images/Rally-spot-2-pano.jpg)

Viñemos aquí porque enfila un chao que esperamos veñan algo lanzados despois da subida, e tórce de dereitas subindo unha curva medio rápida e con pinta de cabrona.  A ver que tal se da.

![Spot 2 · Curva](/images/Rally-spot-2-curva.jpg)

Xa asentados pasa a derradeira asistencia, e detrais un _dos verdes_. O de tráfico íase partindo o cú cando o animamos jajajaja. O especialista pensou que o estaba grabando pero resultou que non lle deu ó botón a tempo e grabou as nosas risas de despois XD. Unha pena, ese vídeo ía ser épico.

---

> 13:48 GMT +1

Da un sol ben xeitoso, e sentimos outra sirena ó lonxe. Mentras agardamos temos posta a trasmisión en directo e oímos nela que tanto senra coma o ruso montaron mollado, pese a que as condicións non son as idóneas para ese neumático. Ás 13.57 e mentras escribo isto pasa outra sirena, e ahí van estes dous apartar un arbol que se deron cuenta que sobraba

:laughing:

![Spot 2 · Pisteros vs Árbol](/images/Rally-spot-2-arbol.jpg)

---

> 14:08 GMT +1

Sentimos arrancar ó primeiro, ben puntual. O sitio é _caralludo_, según pasa Senra a gas xa vemos ó segundo do outro lado do río. Cóntanos o da casa, aínda coa funda posta él e mais o fillo, que pola mañá foise un na curva contra os eucaliptos. Nótaselle a emoción cando nos conta que o sacaron eles despois de acabar o tramo co tractor.

Case sin tempo a que acabe a historia pasa o ruso como unha bala de francotirador. Levántache o corazon velos pasar _a fuego_ e o silbido dos turbos ao baixarlle dúas xuntas antes de estrincar o pé para subir. O turbo do ruso ten un son moi diferente aos demáis.

Xa se sinte o seguinte e pasa tan cerca do ruso que logo non da tempo a escribir. Síntense calar os motores cando dan a curva de abaixo do río, na que estivemos o ano pasado. Según pasa o cuarto vemos vir a expedicion da panadería campo a través. Este deu a curva ó animal, e comenta o da funda que o R5 de pola maña fixo unha maniobra parecida e foise.

Ahí ven outro, pero este foi ó seguro. Este sitio mola moito mais pero é casi imposible fixarse nos números aquí jajaja. Este subeu ben pegado ao lado de fóra, a pouco mais meténdose na cuneta. O especialista vense arriba según pasa e arranca cámara en man ao muro donde apartaron o árbol.

O skoda este pasou agazapado coma se fose nun _scalextric_ sin moverse un milímetro curva arriba. Vaia cracks.

O seguinte xa enfilou ben por dentro, como pasan de lanzados por aqui la virgen. Xa dice o _capitán funda_ que agora estan dando moita brasa. A ver cantos chegan ó churrasco da noite.


Pasa outro que casi mete a esquerda traseira na cuneta ó subir, estivo moi cerca. Logo non teño tempo a apuntar, e xa asoma o seguinte. Un EVO petardeando que nin as _fallas de Valencia_, din os entendidos _"a tres pistós ou?"_. _"Ese vai atravesar tamén!"_ e dito e feito.

Ven detrais un C2 que enfila forzando abondo, pero ó torcer aguanta ben a trazada sin maior problema.

O 208? que pasa despois deuna bastante rápido e sen problemas tamén.

O seguinte arrimouse á cuneta de fóra que xa pensamos que había que arrancar o tractor de volta.

A sensacion cando frenan e adrenalínica, o silbar dos turbos antes de torcer para enfilar a subida lémbrame ós foguetes silbando ceo arriba para estoupar á novena. Desde logo acertamos co sitio.

Pasa un BMW querendo asegurar pero inda así _lanchea_ ó subir que nin dios. Desde logo os tracción traseira son bestas máis difíciles de guiar. Teño un oco e escribin o párrafo explicando o silbido, porque nos primeiros non me deu tempo prácticamente a nada.

O seguinte en pasar volve poñer a duda dos _tres pistós_, porque tarda abondo en chegar desde que o vemos do outro lado do río. Pasa lento, dame tempo a verlle o número 22. Deben ter algún problema no Ibiza.

A continuación pasa un Saxo saltando que nin o meu ZX, ía arreando todo o que daba. Parecía 23 ou 24 pero pode que fose o 23 polo son, lembrando o dos VTS de pola maña. 

Pasa o 25 rectificando moito, parecía que iba saltar pa afora pero non.

Oímos na radio do coche da organización que sale o 28, miro na lista e pon 106 GTI. Pasa coas catro postas, leva pouco peito pero inda sigue.


O seguinte era o nº 30, según pasa oio na radio _"22 meta. Sale 32"_. Tarda algo en chegar, e pasa sin pena nin gloria pero dándolle o que da o 106 _a secas_.

Xusto antes de bruar a seguinte saída pareceme oir ~~_"Sale 36"_~~ pero era o nº 33. Pasa dando o gas que pode coma os outros 106.


Despois pasa o nº 34 o suficiente a modo como para ler Segade e Muñiz. A continuación o nº 37 da fariña coas catro postas tamén, e o seguinte ven moi pegado. É o nº 24, outro con llamas naranjas horteras. A radio di _"28 meta"_.

Pasa o 39 dando o gas que pode mentras o animamos. Era amarillo, sería o que me fixei á maña?

Sentimos ao seguinte dándolle brea, e ahí aparece o 41. Lara debía ir cantando perfecto os detalles que subiron esquivando o ramallo que agora xa non está porque o quitaron.

_"32 meta, sale 44"_. Maria tamén debe cantar ben as curvas que xa os vemos entrar no puente a fuego do outro lado do río. Pasan ó que da o coche, moito mola este sitio.

Os que veñen detrais debe coñecelos a da organización, que atravesa a pista con tempo antes de que cheguen para grabar a pasada. Era o nº 46 o outro BMW que subeu mellor que o primeiro sin atravesar moito, pese a ter tracción de atrais.

Oio na radio _"37 meta"_, e un un vindo a 3 cilindros pero dándoo todo. Era o nº 42, ía ó que daba e non ha ter fallo que a luna traseira patrocinábaa _Recambios Rivas_. Deixa un cheiro a gasolina ó pasar que da gloria.

Paréceme oír ~~_"sale 45"_~~  pero non, era o nº 47. Pasan con medo a romper o EVO, seica deben ir caros.

Oímos ó lonxe o seguinte cortando de mais, din os entendidos que ahí falla algo. É o sierra, débelle fallar o turbo, sube lancheando medio ben pero nótase que vai facendo o que pode.

Parécenos que sona o MK acercándose pero non, era o nº 51. Cheira a queroseno o 306 GTI ese, igual viña despedíndose xa do campeonato.

O nº 43 ven botando fume ata pola ventanilla, parece que ven de paseo co corsita pero pasa dando todo o gas que da.

O nº 55 pasa sonando a silencioso, óese máis o que da saída á vez. Anda ahí o helicóptero controlando. Moito tarda o seguinte, o nº 56 que reza _Concello de Xove_ no capó. Ver volar ó helicóptero é u  espectáculo, parece que está facendo piruetas.

O nº 57 pasa dando o que pode, nótase a diferencia de categoría. O 58 enfila o chao sonando potente o turbo e sube sin maior problema.

A continuación vemos o Escort MK, ese si que é un coche guapo. Sube tentándolle abondo ó eixo de atrais, sen perigo pero con tento.

Pasa o nº 50 que leva unha bandeira _yanki_ no capó, sigue a faltarlle un cacho de coche pero ahí vai dando brea ata que rompa todo.

O seguinte arma un estarabouzo que nin dios ó lonxe. Era Brais co nº 60, ahí apoiando á comarca. Sentiuse clavar freno antes de enfilar, seica entraba vendido, e ó torcer subeu arrimando abondo ó exterior.

_"43 e 55 meta"_. Pasa o suzuki do nº 62 sin peito xa. Danos tempo a comentar chorradas, e acordámonos de Roca do ano pasado, este ano non se apuntou. Hai abondo hueco e volvo oír algo de _"55"_, igual non era meta antes. O seguinte tarda abondo tamén e falamos do fulano que o ano pasado obrigou a interceptar un tramo, menudo personaje. Oímos saír ó seguinte.

Pasa o nº 66 arrimando fóra que apouco mais hai que ir polas slingas. Despois dun cacho pasa o Civic co rótulo de _Tintalúa_ detrais, deume tempo a sacar un pitillo que tiña liado pero non a encendelo. Inda non oio o seguinte, pero a radio dice _"sale 69, 60 meta"_. Dalle Brais.

_"Sale 70"_ mentras tanto pasan Rúa e Cabarcos. _"62 meta, sale 71"_.

Ahí ven 70, un xara. Levaba a aleta tocada. Nótase que fan a de esquivar o árbol pero á noite xa van saber que non está ahí.

_"Sale 73"_ e  enfila o punto de _Agrobicho_ o chao. Vemos o corsita amarillo que fixo a xogada do maletero do outro lado do río, e sentimos a un pasarse monte arriba, na horquilla de pola maña.

O corsita ven coas catro postas e arrimou fóra ó subir. Vese pesado e cóstalle. Suxire o especialista meterlle unhas bridas no escape XD.

Pasa o primeiro pandita co nº 74 saltando e medio de lado, naide entende como agarra á pista. Ó nº 75 cóstalle subir, lento pero seguro. Os panda siguen _en carrera_ jeje.

_"37 meta"_. Preguntolle á da organización pero seica queda algún. Debeume vacilar pola de antes, eso pásame por berrar ó lonxe. Fáltame o paraugas de cores de _pelo largo e barba_ posto na cabeza jaja. Cánta ó lonxe a organización que solo faltan os escobas, non era vacile logo.

> 15:22h GMT +1

Haberá que esperar á noite. Arrancamos de volta e chegando ó fondo vense as manchas de aceite na carretera. Cando chegamos á curva onde estivemos o ano pasado vemos as rodeiras e queda claro que algunhos arrrimaron ó río o que non está escrito. Literalmente non hai unha micra, algún pasou roda polo aire sobre o precipicio.

![Rodeira despois da ponte](/images/Rally-spot-rio-1.jpg)

De paso que lle mandamos un bocadillo e un refrixerio no lugar máxico anterior, entéremome por un rapaz que estivo nesa curva que houbo 3 que pasaron moi a ras, que non se botaron ó rio de casualidade. Menos mal, eu renegaba da curva esa porque non pasan moi rápido ahí, e si se bota un fóra e non o vexo en directo _manda carallo_.

![Curva da ponte sobre o río](/images/Rally-spot-rio-2.jpg)

---

Durante a revisión da crónica comprobei que só [57 coches marcaron tempo no TC4 (Vicedo)](http://temposfga.es/wp-content/uploads/15_A-2-1.pdf) (un menos que no TC3), [52 no TC5 (Xove)](http://temposfga.es/wp-content/uploads/17_B-2-1.pdf), e [50 no TC6 (Cervo)](http://temposfga.es/wp-content/uploads/19_C-2-1.pdf). Un total de 22 coches menos dos 72 autorizados a saír.

---

> 17:17h GMT +1

A espera no merendeiro foi amena e a cháchara entretenida, pero o especialista xa está _que non caga_ e despois de coller suministros arrancamos de volta para o mesmo _spot_ onde vimos o TC6. Pareceunos que o sitio podía estar ben para velos de noite tamén.

Ás 17.38h chegamos ó lugar, e enterámonos por teléfono de que na pasada anterior se foron dous en Gondrás. Viñemos abondo cedo, e toca esperar un bo anaco. Xa andamos máis animados por culpa das mil, a ver si non damos máis espectaculo nosoutros que os coches que agora hai público infantil tamén. Entre os vehículos de asistencia deste tramo pasa un con altavoces chamando á responsabilidade do público, todo moi correcto.

:thinking:

Conectamos a retransmision pero non vai fina desta volta, salta por veces. O pouco do que nos enteramos é de que Senra semella ir conservador e perdeu vantaxe, e que o ruso montara neumáticos de seco.  A peque fainos máis lixeira a espera, contándonos sobre un futuro imaxinario nunha praia tropical con palmeiras, mentres cae o sol paseniño entre as silveiras. 

---

> 18:10h GMT +1

Faltan 12 minutos para a hora marcada e estamos a pensar que agora vai se complicado identificar os coches, sobre todo os do final. En calquera caso tanto da, é o colofón dunha xornada emocionante e divertida.

Cada minuto que pasa baixa un chisco mais a luz, e ás 18.14 pasa outra _sirena_. Se non é a ultima debe ser a penúltima. Decía o especialista que os íamos ver con luz pero os primeiros xa van ter que lidiar co lusco fusco como pouco. Antes de saír o primeiro xa encende a farola, e a cor da luz xa é máis de fusco que de lusco.

![Lusco fusco](/images/Rally-spot-3-farola.jpg)

---

Está o _capitán funda_ de pola maña cantando o bingo, pero inda non sentimos a saída. Estamos ansiosos, boureámoslle á organización que lle dea voz á radio e ahí sentimos...

_"SALE UNO!!!"_


Síntese dar gas e vemos uns faros pasar coma centellas do outro lado do río. Pasa bastante mallado, non tan rápido como a mediodía pero enténdese que sen luz é o normal. Inda non oimos ó ruso, queda moito espacio no medio, só se oe a Senra subindo a Senra, valga a redundancia XD.

Escoitamos a saída do segundo, pero non parece que sone ó ruso. Cando pasa comprobamos que si polo silbido inconfundible. Sona moi becerro subindo ó lonxe, e deberon espaciar mais a saída neste último TC hai moito oco entre coches.

_"Sale tres"_. Caamaño pasa _largao_, aprentado abondo. Mola a sensación da cegada según enfilan o chao ó lonxe, e agora hai máis xente e barullo.

> 18:34h GMT +1

_"Sale cuatro"_. Pasou coma o tres, dándolle brea. Subeu polo sitio e sin botarse, ben lanzado.

Sona unha bramada aló e debe ser o seguinte. Xa é noite pecha casi, pero queda algo de luz. Déixalle ir un pouco ó cu pero baixa o pé e sube dando gas coma un tren pegado á via.

Agora teóricamente ven no que canta Avelino, e efectivamente di a radio _"Sale seis"_. Xa sona saíndo, comenta o especialista que estes teñen un _onboard_ do ano pasado. Viñan apretando moito, deben ter os tempos nun puño.

Non oín ben si era o sete, baixa o chao silbando que nin dios e sube pisando hasta onlle deixa. Van na llama para ser noite. O seguinte non me deixa acabar a frase anterior, xa se mestura o son do que sae co do que pasa.

Vemos ó lonxe un que lle cantan os freos e sona a _fuegos artificiales_. Este silbou que nin dios ó frenar e arrimou á cuneta subindo todo o que lle deu.

Xa vexo do outro lado do rio o seguinte, que entrou cagando ostias coma o resto e a fondo.

Non podo escribir que xa vexo enfilando as luces do seguinte. Debía ser un 208, calquera os conoce sin luz. iba igual de lanzado que os anteriores.

Pasa un fiesta a todo o que daba o motor. Agora xa veñen moi cerca como a mediodía.


Pasa o que parece un Ibiza ou C2, cunha barra de led atravesada, e dice _melenas_ que sonaba a diésel. Parecia xa da seguinte categoría, hai algo de hueco e ven outro, a ver si da tempo a abrir unha.

Pasa un con pouca luz, parecia que ía sin a defensa de diante.

_"Catorce meta"_ e pasa outro saltando coma un _lowrider_. Subeu ben pero frenou apurado.

Pasou un que parecia o 205 dos 3 cilindros. Debeu apuntar que sacaron o árbol, cerrou moito ó subir e deuna máis rápido que os que a dan abrindo.

O seguinte non parecia seguro pero algo debeu anotar tamén. Nin da tempo a escribir e xa ven outro atravensando o río.

Era un 106, pouco barullo pero pasou rápido para a categoría de coche que é.

Temos un oco para abrir unha rápido e xa pasa outro cunha barra de led de lado a lado. _"dieciocho y doce meta"_.

Pasa outro cun foquerío redondo e xeitoso do que da gusto ver - non como o led de merda - e dándolle ben.

Sona a que ven un da compra detrais, moi floxo. Ven coas 4 postas, e 4 focos no capó. Berra o especialista que era un saxo dende o interior da curva.


Pasa outro saxo coas 4 postas pero este inda ía dándolle, parece que o problema non era grave.

Pasa un amarillo, e grazas que vin o color XD. Parecía un saxo. Pasa outro saxo, capó blanco, dándolle cera. sona o que parece un 206 ó lonxe.


_Era un 208_ dice o melenas. Subeu moi lanzado e torceu casi tarde, pero no. Estes non apuntaron o árbol me parece.

Pasa o primeiro dos BMW sin freno de diante xa, botando chispas a do conductor.

Despois o que parece un 205 dándolle brea e cortando gas, non chispeaba pero polo son moito freo non debe levar xa.

Deume tempo a facer un pitillo, e pasa un Lancer EVO conservando.

O seguinte pasa coas catro e sona a que quedou sin cambio, volve o cámara especialista da jungla decindo _"a última pasada en segunda!"_.

Pasou un que _facía pouco ruido_, como diría un entendido en coches coma min, e dícenme que era un corsa. 

Pasa un 106 sin pena nin gloria, con outra barra de led desas feas. Pasa outro 206 e _"sale 57?"_ non oio ben os números. Pasa un clio e sona un Punto detrais. Estes son 57 e 58 claramente.

Ven o escort detrais, e o certo é que son guapas ata as luces dese coche. A peña está animada.

Detrais del ven o 206 rebentado de pola maña. Non dou escrito e xa pasa outro mais. Chivame _melenas_ que era un 106.

Pasa unha cafetera camiño do supermercado e ven outro detrais que a ver si me deixa escribir. A cafetera seica era un swift e detras vai o civic da telaraña que inda ía dándolle.

O saxo que pasa despois iba de paseo tamen. Sexan estes comentarios con respeto claro está, as comparacions sempre foron odiosas, pero hai quen lle dá brea e quen non lle da tanta.

Sona outro becerro ó lonxe. Pasou e era un Punto, e apunto e punto. Órdenes de _barba e melenas_, que non trouxo o paraguas, seica quedou no coche XD.

o seguinte que pasa tivo que tocar o embrague para dar subido, pero non traía as 4 postas.

> 19:24h GMT +1

Díceme o especialista _"Vén un panda!"_ e eu dígolle _"non oh non é un panda"_ e carallo si era! Abríndolle a saco. Unha vez pasa, bérranos a da organización ó lonxe: _"A este que ven hai que animalo todos! Chámanlle manguito!"_ Pensei que sería o coleguilla que grabou pola mañá, e contéstolle _"E logo é o BMW que falta?"_ e vólveme vacilar: _"Si! Unha bala roja!"_.

Eu dinlle por ben feito pero non era BMW nin gaitas en vinagre, o Manguito conducía un Panda e según o vemos enfilar o chao empezamos a berrarlle coma si non houbese un mañá. Según chega á frenada e a grito de _"VAMOS MANGUITO!!"_ veuse arriba tensandolle e subeu pola curva bandeando que a pouco máis vai fóra o animal, molou moitísimo. Sen dúbida a pasada máis épica da noite, perfecto porque foi o último coche en pasar.

> 19:27 GMT +1

Antes de arrancar de volta houbo que facer asistencia técnica á pequena _estoupapozas_ jejeje. Feito o cambio de neumáticos de mollado a seco, cortesía da nai, recollemos e imos arrimarnos á estufa do lugar especial habitual, sen présa pero sen pausa, inda co corazón nun puño e comentando a pasada do Manguito.

---

Mentres remato esta crónica días despois comprobo que [**só 43 coches** acabarían este TC9 (Cervo)](http://temposfga.es/wp-content/uploads/28_C-3.pdf), sendo [46 os que marcaron tempo no TC8 (Xove)](http://temposfga.es/wp-content/uploads/27_B-3-1.pdf), dos [49 que rexistraron no TC7 (O Vicedo)](http://temposfga.es/wp-content/uploads/23_A-3-1.pdf).

43 heroínas e heroes que rexistra a [clasificación final oficial](http://temposfga.es/wp-content/uploads/33_FINAL_OFICIAL-1.pdf), na que figuran penalizacións de tempo a catro deses 43 vehículos. Tamén figuran os 29 abandonos, entre os que destaca por desgracia o [accidente](https://www.elprogreso.es/articulo/a-marina/susto-rallye-marina-accidente-adrian-diaz-andrea-lamas/202012121928281475183.html)  do que me enteraría ao día seguinte pola mañá no xornal, no que tiveron que excarcelar á copiloto dado o aparatoso do caso. Deséxolle dende estas liñas que se recupere.


