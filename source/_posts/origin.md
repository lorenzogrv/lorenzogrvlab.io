---
title: Ørigin
date: 2020-11-25 23:28:11
tags: Filoladas
---

Este post pretende ser a **orixe**.

A **orixe** é aquilo onde algo empeza, e todo - na **orixe** - é nada.

Para darlle un toque a este _post_ vou tomar prestado algo que eu mesmo redactei
hai anos - no 2016 - para o único número que veu a luz do #AFXpress.

> A luz que todos levamos dentro é máis forte na escuridade plena,
> coma a tinta escorrendo no papel branco,
> coma as ondas vibrando o espectro sonoro no silencio.
>
> A maxia que une o todo e a nada.

Quero aproveitar a ocasión para facer _fe de erratas_ da #filolada que figura
sobre estas liñas.

É obrigado decir que quen me abreu os ollos acerca da falta de exactitude da
última frase foi un bo amigo con nome de árbore, que ademáis cumple anos nestes
días.

> **A maxia que une o todo, é a nada**

L.
