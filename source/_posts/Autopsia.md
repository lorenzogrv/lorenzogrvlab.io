---
title: Ăutopsia
date: 2020-12-06 14:34:29
tags: Informática
---

Hoxe decidín, aproveitando uns días libres, revisitar algún proxecto dos que teño no tinteiro, que poucos non son...

:facepalm:

A _revisita_ acabou en exercicio de análise para decidir cal dos proxectos revisitar, pero esa historia é para despois. O motivo desta entrada é que o exercicio de análise acabou en **autopsia**....

![Autopsia, begin](/images/Autopsia-begin.jpg)

Hai xa uns meses amousou síntomas de EOL (_End of Life_). Algún tema de alimentación que lle impedía cargar a batería, e mesmo era preciso sacarlla para poder conectar AC e arrancar. O seu estado fixo que deixase de usalo, ata que hoxe o quixen acender e só chiscou un triste e breve adeus azul do led de encendido.

:broken_heart:

Incontables horas en moitos lugares merecen un pouco de solemnidade, e quixen darlla a través deste _post_ máis ladrillo do habitual, pola cantidade de momentos que esta máquina compartiu comigo.

---

## @shitbook

É o nome dun _netbook_ que merquei no ano 2011, en pleno 15M. Daquela traballaba na Coruña, e a ocupación das plazas colleume sen portátil. Sempre fun de torres, pero lóxicamente para cambiar o mundo dende a plaza do obelisco precisaba algo portable, e collín o trebello máis asequible que vin na FNAC. Un Aspire One moi modesto que montaba Intel Atom e un triste giga de RAM. Co tempo, esta carencia de recursos fixo que o bautizase como _shitbook_.

Daquela os meus coñecementos de Linux eran os xustos, pero xa había tempo que renegara completamente de Windows ata o punto de non telo nos meus equipos. O primeiro que fixen ao saír da tenda foi ir ao piso a instalarlle Ubuntu e _deletear_ calquera traza do Win 7 que traía de serie. Naquel entonces era o sistema que usaba tanto na casa coma no traballo, perfecto para iniciados coma min.

[dia]: https://en.wikipedia.org/wiki/Dia_(software)

Feito isto, arranquei para o obelisco sen saber que aquel sería o día en que debuxaría **o organigrama** no [dia], unha aplicación que me descubriu un compañeiro da que despois sería a Comisión de Comunicación. O curioso do asunto é que dalgunha maneira aquilo marcou ata o punto de convertirme no _tipo do organigrama_. Unha comedia.

:satisfied:

> Neste trasto foi onde debuxei **o organigrama** de Acampada Coruña, cousa épica onde as haxa.

Lembro a M. interesarse polo hardware en canto o sentei no colo e o acendín, e tamén que me preguntou qué opinaba de _unity_, que acababa de saír. Eu daquela tiña `0` idea, era moi _newbie_ e pouco lle puiden aportar. Días despois nunha de tantas conversas entereime que tiña un blog bastante xeitoso [no que segue a escribir](https://www.ubuntizando.com/author/miguel_parada/).

---

[i3]: https://i3wm.org/
[tiling window managers]: https://en.wikipedia.org/wiki/Tiling_window_manager

Anos despois, cando decidín emanciparme de mamá debian e botarlle o ollo a Arch, foi a primeira máquina onde instalei Manjaro. Seguía sendo torpe - e vago - como para montar un Arch de verdade, e o coidada que está Manjaro segue a gustarme ata hoxe. Nesta máquina foi tamén onde probei [i3] e coñecín o concepto dos _[tiling window managers]_, e o certo é que sigo a usar i3 hoxe.

Manjaro deulle unha segunda vida, e corría bastante lixeiro pese ao limitado dos seus recursos. Desta época lembro un momento moi entrañable, aprendendo a usar o `find` de GNU a base de ler os _manpages_ en `man`. Era inverno e estábamos en plena montaña, ao aire libre e ao carón dunha fogueira. Polo lembrar á perfección aquela sensación cálida, o _crap crap_ do lume, e o aroma da leña de carballo facendo brasa...

:heart:

---

Volvendo ao fío, que me vou polas ramas, o caso é que @shitbook xa está para o arrastre, ata o punto de que ter que sacarlle o disco para poder bucear nos seus recordos, e chega o momento de _operar_.

Non é que a operación sexa complexa, pero vencer a ansia de procrastinar ten o seu aquel.


![Autopsia, trail](/images/Autopsia-trail.jpg)

[tornillo]: https://academia.gal/dicionario/-/termo/busca/tornillo
[parafuso]: https://academia.gal/dicionario/-/termo/busca/parafuso

Sen dúbida **autopsia** era o termo máis adecuado. Tomeime o meu tempo, e deixei cada _[tornillo]_ - [parafuso] para os da Real Academia de Gaiteiros - no seu sitio.

Mesmo saquei un par de fotos para probar de paso a ilustrar un _post_ e practicar algo de _branching_ dende @miae.

Antes de conectar os recordos do finado para bucear neles, un `dmesg -w` deixame boas vibracións ao respecto da saúde do disco. Non podo evitar lembrarme do sensei despois dun `lsblk` e ver o tamaño que deixei no seu día para a _swap_...

```shell
$ lsblk
sdb      8:16   0 476,9G  0 disk 
├─sdb1   8:17   0   200G  0 part /run/media/lolo/bbfdbbdb-3192-4a23-9820-1fd1cd38ef9a
├─sdb2   8:18   0     1K  0 part 
├─sdb5   8:21   0   100G  0 part /run/media/lolo/REPOS
├─sdb6   8:22   0   170G  0 part /run/media/lolo/MUSIC
└─sdb7   8:23   0   6,9G  0 part
```

Para onde ía eu con 7 gigas de swap?

:joy:

Sorpréndeme ver o `ls /run/media/lolo/REPOS`, pero iso é parte doutra historia.

Acabarei facendo unha sección de dispositivos, e entre outras máquinas terá o seu lugar especial @shitbook.

> Descansa en Paz

L.

