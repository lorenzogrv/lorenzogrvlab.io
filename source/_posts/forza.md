---
title: ʄorza
date: 2021-01-21 07:42:01
tags: Filoladas
---

Demasiados días sin escribir, dichosa crónica...

:facepalm:

A forza de _voluntá_ é unha **actitude** que debo seguir a entrenar.
