---
title: "ΐåΐ" # - ίåί - ϊåϊ"
date: 2022-09-15 03:34:45
tags: ["Filoladas", "Informática"]
---

:thinking:

Xa hai uns cantos anos - buf, 7 de Febreiro do 2016 - escribín sobre a
[_philosophy behind the iai term_](https://github.com/lorenzogrv/iai/wiki/Concept),
o "Concepto" do "PROXECTO" - con maiúsculas. Maiúsculas porque _iai_ é para min
algo así como un proxecto de vida. Algo importante, por iso ten ese nome.

> The term iai is taken from the japanesse martial art iaido. Under the hood
> it has a deep philosophical meaning. iaido is the path of personal
> reintegration, the fight against the ego to reach the purity of mind,
>
> The union of mental and physical entities to reach the harmony of the
> spirit.
>
> The iai term comes from two ideograms. i means spirit and ai means union,
> harmony. Together, the spirit union or the spirit harmony.
>
> The word is short, symmetric, and easy to read. It's just beautiful,
> and suggests quickness. This attribute built the slogan:
>
> _As fast as say "iai"_

---

Por aquel entón tiña claro que unha ferramenta dixital orientada a manter o
enfoque era "a maneira" de ir máis rápido. Pensaba que a _ferramenta
definitiva_, a que eu ía crear, serviría para _hackearme a min mesmo
mentalmente_. Era "o que necesitaba"...

Sen quitarlle valor ao potencial que ten a ferramenta, **o que non entendía
era que ir máis rápido non significa "chegar antes"**.

---

Sexa como for, redactei textualmente que a clave era unha _mental attitude_, é
decir, _un estado mental que implica creencias, sensacións, valores, e
decisións; de cara a actuar de formas concretas_. Mesmo cheguei a definir a
**iai attitude** (actitude iai) como a crenza persoal na seguinte afirmación:

> Training through repetition is the path to master a technique

Ou o que é o mesmo _A práctica fai ó mestre_, _El hábito hace al monje_,
etcétera. Inda apostillaba que ese é o motivo polo que iai é unha actitude,
pois **implica crer na nosa habilidade innata para mellorar**.

---

Hoxe, lendo o que eu mesmo escribín, asáltanme preguntas variadas entre as que
costa apartar o ruído para ir ao esencial. E con ir ao esencial refírome a
algo que me dixo unha vez un ser a quen amo profundamente e que me chegou moi
adentro:

> A calidade da vida que vivimos é a calidade das preguntas que nos facemos

Sutilezas como que non é unha cuestión de a onde ir, senón dende onde vas; ou
que non importa porqué camiñas, senon para qué camiñas.

---

Hoxe vexo palabras escritas por min hai anos e sorpréndeme a mensaxe. Paréceme
que entre as liñas hai detalles que nin por asomo tiña interiorizados daquela.
Sabía que **automatizar procesos** é o acelerador, e que repetir é clave para
automatizar, mais paréceme que me perdín tentando xustificar sobre una cuestión
de método sen pararme a observar a importancia da **disciplina**.

Certo é que no camiño estaba, sexan as que sexan as palabras, que de feito non
importa ningunha das dese _iai Concept_ sin o verdadeiro poder que
[olvidei anotado na portada](https://github.com/lorenzogrv/iai/wiki) e hoxe
aparece ante min: as instruccións para [**decidir**](https://github.com/lorenzogrv/iai/wiki/Decision-Procedure).
E dice textualmente a primeira liña:

> This procedure has one and only one goal: to put on practice the concept.
>

> Apart from philosophy and excuses (see below), the process is simple:
>
> 1. Observe and identify
> 2. Research and decide
> 3. Review and evolve (repeating 1, 2 and 3)
>
> The key to maintain alive that cycle is to lose the fear of making mistakes
> while maintaining the ability to identify them.
>
> It's not that easy, but it's possible.

Resulta que escribín sobre cómo **tomar decisións** o 30 de Xaneiro do 2016,
antes que o _Concept_, pero só imprimín un papeliño - que inda conservo e teño
na parede da habitación - en lugar de dous. E só con conceptos, todo queda nun
plano abstracto. Levo tempo mirando só unha parte da ecuación: Faltábame -
e fáltame - **práctica**. Todo é poñerse

---

> Instruccións para decidir:
>
> **Observa, Investiga, Decide, Evoluciona, e repite continuamente.**
> **Perde o medo a equivocarte, e aprende dos teus erros.**

---

Grazas por deixarmo anotado

L.

:heart:
