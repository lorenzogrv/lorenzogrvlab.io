---
title: ȹedido
date: 2021-01-22 07:22:26
tags: Música
---

Hoxe chega, teóricamente, o **pedido**. E qué pedido. Nin lembro o tempo que
levaba sin mercar algo levado pola musa música...

:thinking:

Creo que a última vez, sin contar algún pack de cordas ou merdas así, foi a
Warwik 415. Xa choveu pois daquela foi cando grabaramos os primeiros cortes de
_The Brosas_ na Coruña, pasan os anos e perdo a conta supoño.

Desta volta a _inversion?_ vai para a parte máis percusiva da alma, a ver qué
fai con ela...

:sweat_smile:
