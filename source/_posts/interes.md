---
title: ⌘
date: 2021-03-26 06:43:01
tags: Filoladas
---

Interés teño, e dáme na alma que vai ser un camiño longo e por veces - máis das
que quixera - tenso.

Sexa como for, á final do camiño hai destino, e a viaxe merece o esforzo.

Semella de sentido común adentrarse no descoñecido para descubrir, pois resulta
absurdo quedar a mirar a inmesidade lamentando non ver máis alá.

Vamos avanti, e imos vendo.

:coffee:
