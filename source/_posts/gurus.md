---
title: ɠurus
date: 2020-12-04 21:38:17
tags: Filoladas
---

**ɠurus**,

esa xente que cre ter razón ou saber e non cae na conta e que a súa opinión é
tan *in* - e - válida coma a dos que ten ao redor.

(emoticono de *what?*)

E despois, ... indiferencia?

> Winter is coming

L.
